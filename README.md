# Scoreboard for Sport

## Installation with regular webserver
Requirements:
* Webserver with support for PHP7+ and mysqli
* MySQL or mariadb server

1. Create mysql user and empty database
2. Restore db_template into empty database
3. Copy config.template to html/inc/config.php
    * Add MYSQL server, user, password and database
    * Edit timezone if necessary
4. Copy contents of html subfolder to webserver directory
5. Go to your Website and log in with `Admin` and `1234`.
6. Enjoy!

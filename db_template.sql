CREATE TABLE users(
    id SMALLINT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    pin VARCHAR(60) DEFAULT "$2y$10$F/4PnS2iBJkOA/.94QeSL.qs0cB5IfouDkHnNKqbdQgdLnh.rEIyq",
    region TINYINT(3) UNSIGNED DEFAULT "1",
    admin_level TINYINT(3) UNSIGNED DEFAULT "0"
);

INSERT INTO
    users(name, admin_level)
VALUES
    ('Admin', 1);

CREATE TABLE regions(
    id TINYINT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(30) NOT NULL,
    challenge_start DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    challenge_end DATETIME NOT NULL DEFAULT DATE_ADD(NOW(), INTERVAL 28 DAY),
    challenge_goal INT(6) UNSIGNED NOT NULL DEFAULT "2000"
);

INSERT INTO
    regions(name)
VALUES
    ("Standard");

CREATE TABLE stats(
    id INT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id SMALLINT UNSIGNED NOT NULL,
    region_id TINYINT UNSIGNED NOT NULL,
    exercise_id SMALLINT UNSIGNED NOT NULL,
    count SMALLINT UNSIGNED NOT NULL,
    timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE exercises(
    id SMALLINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(1000),
    region_id TINYINT(3) UNSIGNED NOT NULL,
    value TINYINT(6) UNSIGNED NOT NULL DEFAULT "1"
);

INSERT INTO
    exercises(name, region_id)
VALUES
    ('Liegestütz', 1);

CREATE TABLE user_goals(
    user_id SMALLINT(3) UNSIGNED NOT NULL,
    exercise_id SMALLINT(3) UNSIGNED NOT NULL,
    goal INT(6) UNSIGNED NOT NULL DEFAULT "20",
    PRIMARY KEY (user_id, exercise_id)
);

INSERT INTO
    user_goals(user_id, exercise_id)
VALUES
    (1, 1);

CREATE TABLE about(version VARCHAR(10) NOT NULL);

INSERT INTO
    about(version)
VALUES
    ("1.2.2");
<?php
include('inc/functions.php');
include('inc/config.php');
include('inc/frame_functions.php');
include('inc/nav_functions.php');

start($mysqli);

if (!empty($_POST["count"])) {
    if (!is_numeric($_POST["count"]) || $_POST["count"] < 1 || $_POST["count"] > 50000) {
        $_SESSION["alert_array"][] = array("type" => "warning", "message" => "Ungültige Anzahl ($_POST[count]) übergeben.");
    } else {
        $exercise = get_exercise_by_id($mysqli, $_POST["exercise_id"]);
        $mysqli->query("INSERT INTO stats(user_id, region_id, count, exercise_id) VALUES($_SESSION[user_id], $_SESSION[user_region], $_POST[count], $_POST[exercise_id]);") or die($mysqli->error);
        $_SESSION["alert_array"][] = array("type" => "success", "message" => "Dir wurden " . ($_POST["count"] * $exercise->value) . " Punkte für " . $_POST["count"] . " " . $exercise->name . " eingetragen.");

        header("Location: add_entry.php");
        exit;
    }
}

// Daily goal and score
$exercise_array = array();
$result_goal = $mysqli->query("SELECT exercise_id, goal FROM user_goals WHERE user_id = '$_SESSION[user_id]' AND goal != 0;");
while ($row = $result_goal->fetch_object()) {
    $exercise_array[$row->exercise_id]["goal"] = $row->goal;
    $exercise_array[$row->exercise_id]["score"] = 0;
    $result_score_today = $mysqli->query("SELECT count, exercise_id FROM stats WHERE user_id = '$_SESSION[user_id]' AND DATE(CONVERT_TZ(timestamp, 'UTC', '$timezone_user')) = DATE(CONVERT_TZ(NOW(), 'UTC', '$timezone_user')) AND exercise_id = $row->exercise_id;") or die($mysqli->error);
    while ($entry = $result_score_today->fetch_object()) $exercise_array[$entry->exercise_id]["score"] += $entry->count;
}

top("Wiederholungen eintragen");
nav(build_nav($mysqli), "Wiederholungen eintragen");
start_main();
?>

<form method="post">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h3>Wiederholungen eintragen</h3>
            <br>
            <p>
                <button class="btn btn-outline-secondary btn-block btn-sm" type="button" data-toggle="collapse" data-target="#collapseDailyGoals" aria-expanded="false" aria-controls="collapseDailyGoals">
                    Persönliche Tagesziele ein- / ausblenden
                </button>
            </p>
            <div class="collapse show" id="collapseDailyGoals">
                <?php
                foreach ($exercise_array as $id => $exercise) {
                ?>
                    <svg width="100%" height="25px">
                        <g class="bar">
                            <rect style="fill: hsl(0, 0%, 80%)" width="100%" height="25"></rect>
                            <rect style="fill: hsl(120, 50%, 40%)" width="<?php echo ($exercise["score"] / $exercise["goal"] * 100); ?>%" height="25" x="0"></rect>
                        </g>
                    </svg>
                    <h6 class="small"><?php echo get_exercise_by_id($mysqli, $id)->name . ": " . $exercise["score"] . ' / ' . $exercise["goal"]; ?></h6>
                <?php
                }
                ?>
                <br>
            </div>
            <?php
            $result = $mysqli->query("SELECT * FROM exercises WHERE region_id = '$_SESSION[user_region]' ORDER BY NAME;");
            if ($result->num_rows > 0) {
            ?>
                <div class="form-group">
                    <select class="form-control text-center" name="exercise_id" required>
                        <?php
                        while ($exercise = $result->fetch_object()) {
                            echo '<option value="' . $exercise->id . '">' . $exercise->name . '</option>';
                        }
                        ?>
                    </select>
                </div>
            <?php
            }
            ?>
            <div class="form-group">
                <input type="number" class="form-control text-center" name="count" id="count" placeholder="Anzahl" min="1" required>
            </div>
        </div>
    </div>

    <?php
    $buttons_array = array(5, 10, 15, 20, 30, 50, 100, 200);
    foreach ($buttons_array as $key => $value) {
        if ($key % 2 == 0) echo '<div class="row"><div class="col offset-md-3">';
        else echo '<div class="col">';
        echo '<button onclick="document.getElementById(\'count\').value = \'' . $value . '\';" type="button" class="btn btn-outline-primary btn-block">' . $value . '</button></div>';
        if ($key % 2 == 1) echo '<div class="col-md-3"></div></div><br>';
    }
    ?>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Eintragen</button>
            </div>
            <?php
            // Forward to scoreboard or scoreboard_challenge
            if (check_active_challenge($mysqli)) echo '<a href="scoreboard_challenge.php" class="btn btn-outline-secondary btn-block">Zum Scoreboard!</a>';
            else echo '<a href="scoreboard.php" class="btn btn-outline-secondary btn-block">Zum Scoreboard!</a>';
            ?>
        </div>
    </div>
</form>

<?php
bot();
?>
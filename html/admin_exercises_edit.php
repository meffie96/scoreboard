<?php
include('inc/functions.php');
include('inc/config.php');
include('inc/frame_functions.php');
include('inc/nav_functions.php');

start($mysqli);

if ($_SESSION["user_admin_level"] < 1) {
    header("Location: access_denied.php");
    exit;
};

if (!isset($_GET["exercise_id"]) || !is_numeric($_GET["exercise_id"]) || $_GET["exercise_id"] < 1) {
    $_SESSION["alert_array"][] = array("type" => "warning", "message" => "Keine gültige Übung ausgewählt.");
    header("Location: admin_exercises_list.php");
    exit;
}

// Get exercise
$result_exercise = $mysqli->query("SELECT * from exercises WHERE id = '$_GET[exercise_id]' AND region_id = '$_SESSION[user_region]';");
if ($result_exercise->num_rows > 0) {
    $exercise = $result_exercise->fetch_object();
} else {
    $_SESSION["alert_array"][] = array("type" => "warning", "message" => 'Die ausgewählte Übung existiert nicht, oder du darfst sie nicht bearbeiten.');
    header("Location: admin_exercises_list.php");
    exit;
}

if (!empty($_POST['action'])) {
    switch ($_POST['action']) {
        case "delete_exercise":
            $result = $mysqli->query("SELECT id FROM exercises WHERE region_id = '$_SESSION[user_region]';");
            if ($result->num_rows > 1) {
                $mysqli->query("DELETE FROM exercises WHERE id = '$exercise->id';");
                $mysqli->query("DELETE FROM user_goals WHERE exercise_id = '$exercise->id';");
                $mysqli->query("DELETE FROM stats WHERE exercise_id = '$exercise->id';");

                $_SESSION["alert_array"][] = array("type" => "success", "message" => 'Übung "' . $exercise->name . '" gelöscht.');
                header("Location: admin_exercises_list.php");
                exit;
            } else $_SESSION["alert_array"][] = array("type" => "warning", "message" => 'Dies ist die letzte Übung der Region. Du kannst sie erst löschen, wenn du eine neue angelegt hast.');
            break;

        case "edit_exercise":
            if ($_POST["exercise_name"] != "" && $_POST["exercise_name"] != $exercise->name) {
                $mysqli->query("UPDATE exercises SET name = '$_POST[exercise_name]' WHERE id = '$exercise->id';");
                if ($mysqli->errno != 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => $mysqli->error);
                else $_SESSION["alert_array"][] = array("type" => "success", "message" => 'Name wurde geändert.');
            }
            if ($_POST["exercise_description"] != "" && $_POST["exercise_description"] != $exercise->description) {
                $mysqli->query("UPDATE exercises SET description = '$_POST[exercise_description]' WHERE id = '$exercise->id';");
                if ($mysqli->errno != 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => $mysqli->error);
                else $_SESSION["alert_array"][] = array("type" => "success", "message" => 'Beschreibung wurde geändert.');
            }
            if ($_POST["exercise_value"] != "" && $_POST["exercise_value"] != $exercise->value && is_numeric($_POST["exercise_value"])) {
                $mysqli->query("UPDATE exercises SET value = '$_POST[exercise_value]' WHERE id = '$exercise->id';");
                if ($mysqli->errno != 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => $mysqli->error);
                else $_SESSION["alert_array"][] = array("type" => "success", "message" => 'Wert wurde geändert.');
            }

            // Get changes
            $result_exercise = $mysqli->query("SELECT * from exercises WHERE id = '$_GET[exercise_id]' AND region_id = '$_SESSION[user_region]';");
            $exercise = $result_exercise->fetch_object();
            break;
    }
}

top("Übungen verwalten");
nav(build_nav($mysqli), "Übungen verwalten");
start_main();
?>
<div class="row">
    <div class="col offset-md-3">
        <h3>Übung "<?php echo $exercise->name; ?>" verwalten</h3>
        <?php
        ?>
        <br>
        <form method="post">
            <div class="form-group">
                <label for="exercise_name">Name der Übung</label>
                <input type="text" class="form-control" id="exercise_name" name="exercise_name" placeholder="<?php echo $exercise->name; ?>">
            </div>
            <div class="form-group">
                <label for="exercise_description">Beschreibung</label>
                <textarea class="form-control" id="exercise_description" name="exercise_description" rows="3" maxlength="1000"><?php echo $exercise->description; ?></textarea>
            </div>
            <div class="form-group">
                <label for="exercise_value">Wert</label>
                <input type="number" class="form-control" id="exercise_value" name="exercise_value" min="0" max="255" value="<?php echo $exercise->value; ?>">
                <small id="exercise_value_help" class="form-text text-muted">Die Umrechnung aller Einträge erfolgt automatisch, du kannst sie dir direkt im Scoreboard ansehen.</small>
            </div>
            <br>
            <input type="hidden" name="action" value="edit_exercise">
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Änderungen übernehmen</button>
            </div>
        </form>
        <form method="post">
            <input type="hidden" name="action" value="delete_exercise">
            <div class="form-group">
                <button type="submit" class="btn btn-danger btn-block" onclick="return confirm('Willst du die Übung wirklich löschen? Alle Einträge zu dieser Übung werden ebenfalls gelöscht.')">Übung löschen</button>
            </div>
        </form>
        <?php
        echo back_button();
        ?>
    </div>
    <div class="col-md-3"></div>
</div>

<?php
bot();
?>
<?php
include('inc/functions.php');
include('inc/config.php');
include('inc/frame_functions.php');
include('inc/nav_functions.php');

start($mysqli);

if ($_SESSION["user_admin_level"] < 1) {
    header("Location: access_denied.php");
    exit;
};

// Build exercises-array
$result = $mysqli->query("SELECT * FROM exercises WHERE region_id = '$_SESSION[user_region]' ORDER BY name;") or die($mysqli->error);
while ($exercise = $result->fetch_object()) {
    $result_entries = $mysqli->query("SELECT COUNT(*) as count FROM stats WHERE exercise_id = '$exercise->id';") or die($mysqli->error);
    $exercise->count = $result_entries->fetch_object()->count;
    $exercises_array[] = $exercise;
}

top("Übungen verwalten");
nav(build_nav($mysqli), "Übungen verwalten");
start_main();
?>
<div class="row">
    <div class="col offset-md-3">
        <h3>Übungen verwalten</h3>
        <br>
        <?php
        if (isset($exercises_array)) {
        ?>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Einträge</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($exercises_array as $exercise) {
                        echo '<tr><td>' . $i . '</td>';
                        echo '<td><a href="admin_exercises_edit.php?exercise_id=' . $exercise->id . '">' . $exercise->name . '</a></td>';
                        echo '<td>' . $exercise->count . '</td>';
                        echo '</tr>';
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        <?php
        }
        if ($_SESSION["user_admin_level"] == 1) echo '<div class="form-group"><a href="admin_exercises_add.php" class="btn btn-primary btn-block">Neue Übung hinzufügen</a></div>';
        echo back_button();
        ?>
    </div>
    <div class="col-md-3"></div>
</div>

<?php
bot();
?>
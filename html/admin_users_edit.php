<?php
include('inc/functions.php');
include('inc/config.php');
include('inc/frame_functions.php');
include('inc/nav_functions.php');

start($mysqli);

// Forward for editing own user
if ($_GET["user_id"] == $_SESSION["user_id"]) {
    header("Location: user_settings.php");
    exit;
}

if ($_SESSION["user_admin_level"] < 1) {
    header("Location: access_denied.php");
    exit;
};

// Was a valid user_id given?
if (!isset($_GET["user_id"]) || !is_numeric($_GET["user_id"]) || $_GET["user_id"] < 1) {
    header("Location: admin_users_list.php");
    exit;
}

// Check if a valid user_id was selected
$result = $mysqli->query("SELECT * FROM users WHERE id = $_GET[user_id]");
if ($result->num_rows < 1) {
    header("Location: admin_users_list.php");
    exit;
} else $user = $result->fetch_object();

// Check if current user may edit selected user
if (!may_edit_user($user)) {
    header("Location: admin_users_list.php");
    exit;
}

if (!empty($_POST['action'])) {
    switch ($_POST['action']) {
        case "delete_user":
            $_SESSION["alert_array"][] = delete_user($mysqli, $_GET["user_id"]);
            header("Location: admin_users_list.php");
            exit;
        case "edit_user":
            $user = new stdClass();
            $user->id = $_GET["user_id"];
            $user->name = $_POST["user_name"];
            $user->admin_level = $_POST["user_admin_level"];
            $user->region = $_POST["user_region"];
            update_user($mysqli, $user);

            // Get new data
            $result = $mysqli->query("SELECT * FROM users WHERE id = $_GET[user_id]");
            $user = $result->fetch_object();
            break;
        case "reset_password":
            $hashed_1234 = password_hash('1234', PASSWORD_BCRYPT);
            $mysqli->query("UPDATE users SET pin = '$hashed_1234' WHERE id = $_GET[user_id]") or die($mysqli->error);
            $_SESSION["alert_array"][] = array("message" => 'Passwort erfolgreich auf "1234" zurückgesetzt.', "type" => "success");
            break;
    }
}

top("Nutzer verwalten");
nav(build_nav($mysqli), "Nutzer verwalten");
start_main();
?>
<div class="row">
    <div class="col offset-md-3">
        <h3>Nutzer "<?php echo $user->name; ?>" verwalten</h3>
        <br>
        <form method="post">
            <div class="form-group">
                <label for="user_name">Nutzername</label>
                <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Nutzername" value="<?php echo $user->name; ?>" required>
            </div>
            <?php
            // Admin may change everybodys permissions and region
            if ($_SESSION["user_admin_level"] == 1) {
            ?>
                <div class="form-group">
                    <label for="region_select">Region wählen:</label>
                    <select class="form-control" id="region_select" name="user_region">
                        <?php echo display_region_select($mysqli, $user->region); ?>
                    </select>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="user_admin_level" id="user_admin_level2" value="1" <?php if ($user->admin_level == "1") echo "checked"; ?>>
                    <label class="form-check-label" for="user_admin_level2">Administrator</label>
                </div>
            <?php
            }
            // Regions-admin may only change permissions of users of his region and not admins permission
            elseif ($user->admin_level != 1 && $user->region == $_SESSION["user_region"]) echo '<input type="hidden" name="user_region" value="' . $_SESSION["user_region"] . '">';
            ?>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="user_admin_level" id="user_admin_level3" value="2" <?php if ($user->admin_level == "2") echo "checked"; ?>>
                <label class="form-check-label" for="user_admin_level3">Administrator für seine Region</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="user_admin_level" id="user_admin_level1" value="0" <?php if ($user->admin_level == "0") echo "checked"; ?>>
                <label class="form-check-label" for="user_admin_level1">Nutzer</label>
            </div>
            <br>
            <input type="hidden" name="action" value="edit_user">
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Änderungen übernehmen</button>
            </div>
        </form>
        <form method="post">
            <input type="hidden" name="action" value="reset_password">
            <div class="form-group">
                <button type="submit" class="btn btn-warning btn-block" onclick="return confirm('Willst du das Passwort von <?php echo $user->name; ?> wirklich auf 1234 zurücksetzen?.')">Passwort zurücksetzen</button>
            </div>
        </form>
        <form method="post">
            <input type="hidden" name="action" value="delete_user">
            <div class="form-group">
                <button type="submit" class="btn btn-danger btn-block" onclick="return confirm('Willst du den Nutzer <?php echo $user->name; ?> wirklich löschen? Alle Einträge von <?php echo $user->name; ?> werden ebenfalls gelöscht. Diese Aktion kann nicht rückgängig gemacht werden!')">Nutzer löschen</button>
            </div>
        </form>
        <?php echo back_button(); ?>
    </div>
    <div class="col-md-3"></div>
</div>

<?php
bot();
?>
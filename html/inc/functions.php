<?php
function update_db($mysqli)
{
    // Check version
    $result_version = $mysqli->query("SELECT version FROM about;");

    // Version less than 1.2 (multiple challenges)    
    if ($mysqli->errno != 0) {

        // Update database to use crypted passwords
        $result = $mysqli->query("SELECT * FROM users LIMIT 1;");
        $user = $result->fetch_object();
        if (strpos($user->pin, '2y$10$') === false) {
            $hashed_1234 = password_hash('1234', PASSWORD_BCRYPT);
            $mysqli->query("ALTER TABLE users MODIFY pin VARCHAR(60) DEFAULT '$hashed_1234'") or die($mysqli->error);

            $result = $mysqli->query("SELECT * FROM users;");
            while ($user = $result->fetch_object()) {
                $user->pin = password_hash($user->pin, PASSWORD_BCRYPT);
                $mysqli->query("UPDATE users SET pin = '$user->pin' WHERE id = '$user->id';") or die($mysqli->error);
            }
        }

        // Update database to support multiple exercise types

        // Table exercises
        $mysqli->query("CREATE TABLE exercises(
        id SMALLINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(100) NOT NULL,
        description VARCHAR(1000),
        region_id TINYINT(3) UNSIGNED NOT NULL,
        goal INT(6) UNSIGNED NOT NULL DEFAULT 1000);") or die($mysqli->error);

        $result = $mysqli->query("SELECT id, goal FROM regions;");
        while ($region = $result->fetch_object()) {
            $mysqli->query("INSERT INTO exercises(name, region_id, goal) VALUES('Liegestütz', $region->id, $region->goal), ('Kniebeugen', $region->id, '0'), ('Klimmzüge', $region->id, '0');");
        }

        // Table stats
        $mysqli->query("ALTER TABLE stats ADD exercise_id SMALLINT(6) UNSIGNED NOT NULL;") or die($mysqli->error);
        $result = $mysqli->query("SELECT id, region_id FROM exercises WHERE name = 'Liegestütz';");
        while ($exercise = $result->fetch_object()) {
            $mysqli->query("UPDATE stats SET exercise_id='$exercise->id' WHERE region_id = '$exercise->region_id';") or die($mysqli->error);
        }

        // Table regions
        $mysqli->query("ALTER TABLE regions DROP goal;") or die($mysqli->error);
        $mysqli->query("ALTER TABLE regions MODIFY challenge_start DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;");
        $mysqli->query("ALTER TABLE regions MODIFY challenge_end DATETIME NOT NULL DEFAULT DATE_ADD(NOW(), INTERVAL 28 DAY);");
        $mysqli->query("UPDATE regions SET name = 'Standard' WHERE id = 1;");

        // Table user_goals
        $mysqli->query("CREATE TABLE user_goals(user_id SMALLINT(3) UNSIGNED NOT NULL, exercise_id SMALLINT(3) UNSIGNED NOT NULL, goal INT(6) UNSIGNED NOT NULL DEFAULT 20, PRIMARY KEY (user_id, exercise_id));") or die($mysqli->error);
        $users_array = array();
        $result_user = $mysqli->query("SELECT id, daily_goal, region FROM users;");
        while ($user = $result_user->fetch_object()) $users_array[] = $user;
        foreach ($users_array as $user) {
            $result_exercises = $mysqli->query("SELECT id FROM exercises WHERE region_id = $user->region");
            while ($exercise = $result_exercises->fetch_object()) {
                $mysqli->query("INSERT INTO user_goals(user_id, exercise_id, goal) VALUES($user->id, $exercise->id, 50);");
            }
        }

        // Table users
        $mysqli->query("ALTER TABLE users DROP daily_goal;") or die($mysqli->error);

        // Update database to save version
        $mysqli->query("CREATE TABLE about(version VARCHAR(10) NOT NULL);") or die($mysqli->error);
        $mysqli->query("INSERT INTO about(version) VALUES ('1.2');") or die($mysqli->error);
        $result_version = $mysqli->query("SELECT version FROM about;");
    }

    $version = $result_version->fetch_object()->version;

    switch ($version) {
        case "1.2":
            // Delete built-in user
            $mysqli->query("DELETE FROM users WHERE id = 1;");
            $mysqli->query("DELETE FROM stats WHERE user_id = 1;");
            $mysqli->query("DELETE FROM user_goals WHERE user_id = 1;");
        case "1.2.1":
            // Add region_goal
            $mysqli->query("ALTER TABLE regions ADD challenge_goal INT(6) UNSIGNED NOT NULL DEFAULT 20000;");
            $mysqli->query("ALTER TABLE exercises ADD value TINYINT(6) UNSIGNED NOT NULL DEFAULT 1;");
            $mysqli->query("ALTER TABLE exercises DROP goal;");
            $mysqli->query("UPDATE about SET version='1.2.2';");
            $version = "1.2.2";
        case "1.2.2":
            // Increase count limit
            $mysqli->query("ALTER TABLE stats MODIFY count SMALLINT UNSIGNED NOT NULL;");
            $mysqli->query("UPDATE about SET version='1.2.3';");
            $version = "1.2.3";
    }
    return $version;
}

function start($mysqli)
{
    session_start();

    // Update DB if necessary
    $version = update_db($mysqli);

    // Check if user is logged in
    if (empty($_SESSION['user_id'])) {
        $_SESSION["alert_array"][] = array("type" => "warning", "message" => "Du musst dich anmelden, um Zugriff auf diese Seite zu erhalten.");
        header("Location: index.php");
        exit;
    }

    // Check version
    if ($_SESSION["version"] != $version) {
        $_SESSION["alert_array"][] = array("type" => "warning", "message" => "Der Server wurde gerade aktualisiert. Du musst dich leider erneut anmelden.");
        header("Location: index.php?action=logout");
        exit;
    }

    // Check if cached exercise exists
    if ($_SESSION["exercise_id"] != 0) {
        $result_exercises = $mysqli->query("SELECT id FROM exercises WHERE id = '$_SESSION[exercise_id]';");
        if ($result_exercises->num_rows != 1) {
            $result_exercises = $mysqli->query("SELECT id FROM exercises WHERE region_id = $_SESSION[user_region] LIMIT 1;");
            $_SESSION["exercise_id"] = $result_exercises->fetch_object()->id;
        }
    }

    // Get data of user
    $_SESSION["user"] = get_user_by_id($mysqli, $_SESSION["user_id"]);
    if ($_SESSION["user"]->id == 0) {
        $_SESSION["alert_array"][] = array("type" => "warning", "message" => "Dein Account ist gelöscht worden.");
        header("Location: index.php?action=logout");
        exit;
    }
}


// Get functions

function get_region_by_id($mysqli, $id)
{
    $result = $mysqli->query("SELECT name FROM regions WHERE id = $id");
    $region = $result->fetch_object();
    return $region->name;
}

function get_admin_description($mysqli, $user)
{
    switch ($user->admin_level) {
        case "0":
            return "Nutzer";
        case "1":
            return "Admin";
        case "2":
            $result = $mysqli->query("SELECT name FROM regions WHERE id = $user->region");
            $region = $result->fetch_object();
            return "Admin ($region->name)";
    }
}

function get_user_by_id($mysqli, $id)
{
    $result = $mysqli->query("SELECT * FROM users WHERE id = $id");
    if ($result->num_rows == 1) return $result->fetch_object();
    else {
        $user = new stdClass();
        $user->name = "Gelöschter Nutzer";
        $user->id = 0;
        $user->admin_level = 0;
        return $user;
    }
}

function get_beautiful_timespan($seconds)
{
    $timespan = array();

    // Days
    if (floor($seconds / 86400) == 1) $timespan[] = "ein Tag";
    else $timespan[] = floor($seconds / 86400) . " Tage";

    // Hours
    if (floor($seconds % 86400 / 3600) == 1) $timespan[] = "eine Stunde";
    else $timespan[] = floor($seconds % 86400 / 3600) . " Stunden";

    // Minutes
    if (floor($seconds % 3600 / 60) == 1) $timespan[] = "eine Minute";
    else $timespan[] = floor($seconds % 3600 / 60) . " Minuten";

    // Create complete String
    $timespan["string"] = "";
    foreach ($timespan as $element) {
        $timespan["string"] .= $element . ", ";
    }
    $timespan["string"] = rtrim($timespan["string"], ", ");

    return $timespan;
}

function get_exercise_by_id($mysqli, $id)
{
    $result = $mysqli->query("SELECT * FROM exercises WHERE id = $id");
    return $result->fetch_object();
}

// Is there an active challenge?
function check_active_challenge($mysqli)
{
    $result = $mysqli->query("SELECT challenge_start, challenge_end FROM regions WHERE id = '$_SESSION[user_region]';");
    $region = $result->fetch_object();

    // Create DateTime objects
    $current_date = new DateTime();
    $region->challenge_start = new DateTime($region->challenge_start);
    $region->challenge_end = new DateTime($region->challenge_end);

    // Return true or false
    if ($region->challenge_start < $current_date && $current_date < $region->challenge_end) return true;
    else return false;
}


// Permission functions

function may_edit_user($user)
{
    if ($_SESSION["user_admin_level"] == 1) return true;

    // Special permissions for region admins
    if ($_SESSION["user_admin_level"] == 2) {
        // May not edit Admins
        if ($user->admin_level == 1) return false;

        // May only edit users that are from their region
        if ($_SESSION["user_region"] == $user->region) return true;
    }
    return false;
}

function may_edit_region($region)
{
    if ($_SESSION["user_admin_level"] == 1) return true;
    if ($_SESSION["user_admin_level"] == 2 && $_SESSION["user_region"] == $region->id) return true;
    return false;
}


// Database functions

function add_user($mysqli, $user)
{
    if (!empty($user->name)) {
        $result_user = $mysqli->query("SELECT * FROM users WHERE name = '$user->name'");
        if ($result_user->num_rows > 0) $_SESSION["alert_array"][] = array("type" => "warning", "message" => "Dieser Nutzername ist bereits vergeben.");

        else {
            if (!empty($user->pin)) {
                if (!is_numeric($user->pin)) $_SESSION["alert_array"][] = array("message" => "Der Pin darf keine ungültigen Zeichen enthalten.", "type" => "warning");
                elseif ($user->pin < 1000 || $user->pin > 9999999999) $_SESSION["alert_array"][] = array("message" => "Der Pin muss zwischen 4 und 10 Zeichen lang sein.", "type" => "warning");

                else {
                    $user->pin = password_hash($user->pin, PASSWORD_BCRYPT);
                    if (!empty($user->region)) {
                        $result_region = $mysqli->query("SELECT id FROM regions WHERE id = $user->region");
                        if ($result_region->num_rows > 0) {

                            $mysqli->query("INSERT INTO users(name, pin, region, admin_level) VALUES('$user->name', '$user->pin', '$user->region', '$user->admin_level');");
                            if ($mysqli->errno != 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => $mysqli->error);
                            else {
                                $result_user = $mysqli->query("SELECT * FROM users WHERE name = '$user->name';");
                                $user = $result_user->fetch_object();

                                // Add user goals
                                $result_exercises = $mysqli->query("SELECT * FROM exercises WHERE region_id = $user->region;");
                                while ($exercise = $result_exercises->fetch_object()) {
                                    $mysqli->query("INSERT INTO user_goals (user_id, exercise_id, goal) VALUES ($user->id, $exercise->id, 0);");
                                }
                                return $user;
                            }
                        } else $_SESSION["alert_array"][] = array("type" => "warning", "message" => "Die gewählte Region konnte nicht gefunden werden.");
                    }
                }
            }
        }
    }
    return false;
}

function update_user($mysqli, $user)
{
    $result_user = $mysqli->query("SELECT * FROM users WHERE id = $user->id");
    if ($result_user->num_rows > 0) {
        $current = $result_user->fetch_object();

        if (!empty($user->name) && $user->name != $current->name) {
            $result_user = $mysqli->query("SELECT * FROM users WHERE name = '$user->name' AND id != $user->id");
            if ($result_user->num_rows > 0) $_SESSION["alert_array"][] = array("type" => "warning", "message" => "Dieser Nutzername ist bereits vergeben.");
            else {
                $mysqli->query("UPDATE users SET name = '$user->name' WHERE id = $user->id;");
                if ($mysqli->errno != 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => $mysqli->error);
                else $_SESSION["alert_array"][] = array("type" => "success", "message" => "Nutzername erfolgreich geändert.");
            }
        }

        if (!empty($user->pin) && !password_verify($user->pin, $current->pin)) {
            if (!is_numeric($user->pin)) $_SESSION["alert_array"][] = array("message" => "Der Pin darf keine ungültigen Zeichen enthalten.", "type" => "warning");
            elseif ($user->pin < 1000 || $user->pin > 9999999999) $_SESSION["alert_array"][] = array("message" => "Der Pin muss zwischen 4 und 10 Zeichen lang sein.", "type" => "warning");
            else {
                $user->pin = password_hash($user->pin, PASSWORD_BCRYPT);
                $mysqli->query("UPDATE users SET pin = '$user->pin' WHERE id = $user->id;");
                if ($mysqli->errno != 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => $mysqli->error);
                else $_SESSION["alert_array"][] = array("type" => "success", "message" => "PIN erfolgreich geändert.");
            }
        }

        if (!empty($user->region) && $user->region != $current->region) {
            $result_region = $mysqli->query("SELECT id FROM regions WHERE id = $user->region");
            if ($result_region->num_rows > 0) {
                $mysqli->query("UPDATE users SET region = '$user->region' WHERE id = $user->id;");
                if ($mysqli->errno != 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => $mysqli->error);
                else {
                    // Delete old exercise goals and create new ones
                    $mysqli->query("DELETE FROM user_goals WHERE user_id = $user->id;");
                    $result_exercises = $mysqli->query("SELECT id FROM exercises WHERE region_id = $user->region") or die($mysqli->error);
                    while ($exercise = $result_exercises->fetch_object()) {
                        $mysqli->query("INSERT INTO user_goals(user_id, exercise_id, goal) VALUES ($user->id, $exercise->id, 0);");
                    }
                    if ($mysqli->errno != 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => $mysqli->error);
                    else $_SESSION["alert_array"][] = array("type" => "success", "message" => "Region erfolgreich geändert.");
                }
            } else $_SESSION["alert_array"][] = array("type" => "warning", "message" => "Die gewählte Region konnte nicht gefunden werden.");
        }

        if (isset($user->admin_level) && $user->admin_level != $current->admin_level) {
            $mysqli->query("UPDATE users SET admin_level = '$user->admin_level' WHERE id = $user->id;");
            if ($mysqli->errno != 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => $mysqli->error);
            else $_SESSION["alert_array"][] = array("type" => "success", "message" => "Administratorlevel erfolgreich geändert.");
        }
        
    } else $_SESSION["alert_array"][] = array("type" => "warning", "message" => "Der gewählte Nutzer konnte nicht gefunden werden.");
}

function delete_user($mysqli, $user_id)
{
    $alert = array("type" => "success", "message" => 'Nutzer "' . get_user_by_id($mysqli, $user_id)->name . '" gelöscht.');

    $mysqli->query("DELETE FROM stats WHERE user_id = $user_id");
    $mysqli->query("DELETE FROM users WHERE id = $user_id");
    $mysqli->query("DELETE FROM user_goals WHERE user_id = '$user_id';");
    return $alert;
}

function add_region($mysqli, $name)
{
    // Add region and get id
    $mysqli->query("INSERT INTO regions(name) VALUES('$name');") or die($mysqli->error);
    $result = $mysqli->query("SELECT * FROM regions WHERE name = '$name';");
    $region = $result->fetch_object();
    $mysqli->query("INSERT INTO exercises(region_id, name) VALUES ($region->id, 'Liegestütz');");

    return $region;
}


// Display functions

function display_region_select($mysqli, $current_region)
{
    $result = $mysqli->query("SELECT * FROM regions ORDER BY name");
    while ($region = $result->fetch_object()) {
        $regions_array[] = $region;
    }

    if (count($regions_array) > 0) {
        $select = '';

        foreach ($regions_array as $region) {
            $select .= '<option value="' . $region->id . '"';
            if ($current_region == $region->id) $select .= " selected";
            $select .= '>' . $region->name . '</option>';
        }
        return $select;
    }
}

function back_button()
{
    if (!empty($_SESSION["sites_visited"]) && count($_SESSION["sites_visited"]) > 2) {
        return '<a href="' . $_SESSION["sites_visited"][count($_SESSION["sites_visited"]) - 2] . '" class="btn btn-outline-secondary btn-block">Zurück</a>';
    } else return "";
}

function display_score_table($score_array, $caption, $current_date)
{
    // Delete users without score
    foreach ($score_array as $key => $user) if (empty($user->score)) unset($score_array[$key]);

    usort($score_array, function ($a, $b) {
        return $b->score <=> $a->score;
    });

    if (!empty($score_array)) {
?>
        <table class="table">
            <caption><?php echo $caption; ?></caption>
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Score</th>
                    <th scope="col">Name</th>
                    <th scope="col">Datum</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                $o = 0;
                $lastscore = 0;
                foreach ($score_array as $user) {
                    // If two users have the same score, they will share their placement
                    if ($lastscore != $user->score) {
                        $i++;
                        $i += $o;
                        $o = 0;
                    } else $o++;

                    echo '<tr>';
                    echo '<td scope="row">' . $i . '</td>';
                    echo '<td scope="row">' . $user->score . '</td>';
                    echo '<td scope="row"><a href="stats_user.php?user_id=' . $user->id . '">' . $user->name . '</a></td>';
                    if ($current_date->format('Ymd') == $user->timestamp->format('Ymd')) {
                        $timestamp = $user->timestamp->format("H:i") . ' Uhr';
                    } else {
                        $timestamp = $user->timestamp->format("d.m.Y");
                    }
                    echo '<td scope="row">' . $timestamp . '</td>';
                    echo '</tr>';
                    $lastscore = $user->score;
                }
                ?>
            </tbody>
        </table>
<?php
    } else echo '<div class="alert alert-primary" role="alert">Zu diesem Übungstyp liegen keine Einträge vor.</div>';
}


// Validation functions

function validate_region_name($mysqli, $region_name, $region_id)
{
    $result = $mysqli->query("SELECT * FROM regions WHERE name LIKE '$region_name' AND id != $region_id");
    if (empty($region_name)) $alert = array("message" => "Der Name der Region darf nicht leer sein.", "type" => "danger");
    elseif ($result->num_rows != 0) $alert = array("message" => "Eine Region mit diesem Namen exisitert bereits.", "type" => "danger");
    else $alert = array("type" => "success");
    return $alert;
}

function validate_user_name($mysqli, $user_name, $user_id)
{
    $result_user = $mysqli->query("SELECT * FROM users WHERE name = '$user_name' AND id != $user_id");

    if (empty($user_name)) $alert = array("message" => "Der Nutzername darf nicht leer sein.", "type" => "danger");
    elseif ($result_user->num_rows > 0) $alert = array("message" => "Dieser Nutzername ist bereits vergeben.", "type" => "danger");
    else $alert = array("type" => "success");
    return $alert;
}

function validate_user_pin($pin)
{
    if (empty($pin)) $alert = array("message" => "Der Pin darf nicht leer sein.", "type" => "danger");
    elseif (!is_numeric($pin)) $alert = array("message" => "Der Pin darf keine ungültigen Zeichen enthalten.", "type" => "danger");
    elseif ($pin > 9999999999) $alert = array("message" => "Der Pin muss zwischen 4 und 10 Zeichen lang sein.", "type" => "danger");
    elseif ($pin < 1000) $alert = array("message" => "Der Pin muss zwischen 4 und 10 Zeichen lang sein.", "type" => "danger");
    else $alert = array("type" => "success");
    return $alert;
}

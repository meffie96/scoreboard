<?php
function nav($nav_elements_array = "", $active_nav_item = "")
{
?>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-bottom">
        <a class="navbar-brand" href="https://parkourone.com" target="_blank"><img src="img/logo.png" height="27" alt="ParkourOne"></a>
        <?php
        if (is_array($nav_elements_array)) {
        ?>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <?php
                    foreach ($nav_elements_array as $nav_item => $nav_link) {
                        echo '<li class="nav-item';
                        if ($active_nav_item == $nav_item) echo ' active';
                        echo '"><a class="nav-link" href="' . $nav_link . '">' . $nav_item . '</a></li>';
                    }
                    ?>
                </ul>
            </div>
        <?php
        }
        ?>
    </nav>
    <?php
}

function build_nav($mysqli)
{
    $nav_elements_array = array();

    // Add push-ups
    $nav_elements_array["Wiederholungen eintragen"] = "add_entry.php";

    // Add scoreboard
    if (check_active_challenge($mysqli)) $nav_elements_array["Scoreboard"] = "scoreboard_challenge.php";
    else $nav_elements_array["Scoreboard"] = "scoreboard.php";

    // Add Stats
    $nav_elements_array["Statistik"] = "stats_region.php";

    // Add settings
    if ($_SESSION["user_admin_level"] > 0) {
        $nav_elements_array["Regionen verwalten"] = "admin_regions_list.php";
        $nav_elements_array["Nutzer verwalten"] = "admin_users_list.php";
        $nav_elements_array["Übungen verwalten"] = "admin_exercises_list.php";
    }
    $nav_elements_array["Accounteinstellungen"] = "user_settings.php";

    // Add logout
    $nav_elements_array[$_SESSION["user_name"] . " abmelden"] = "index.php?action=logout";
    return $nav_elements_array;
}

function nav_scoreboard()
{
    $button_array = array("Herausforderung" => "scoreboard_challenge.php", "Monatliche Chronik" => "scoreboard.php");

    echo '<div class="row"><div class="col offset-md-3"><h3>Scoreboard</h3><div class="list-group">';
    foreach ($button_array as $text => $link) {
        echo '<a href="' . $link . '" class="list-group-item list-group-item-action p-2';
        if ($_SERVER['PHP_SELF'] == "/" . $link) echo ' active';
        echo '">' . $text . '</a>';
    }
    echo '</div></div><div class="col-md-3"></div></div><br>';
}

function nav_stats($mysqli)
{
    $button_array = array(get_region_by_id($mysqli, $_SESSION["user_region"]) => "stats_region.php", $_SESSION["user_name"] => "stats_user.php?user_id=" . $_SESSION["user_id"]);

    echo '<div class="row"><div class="col offset-md-3"><h3>Statistik</h3><div class="list-group">';
    foreach ($button_array as $text => $link) {
        echo '<a href="' . $link . '" class="list-group-item list-group-item-action p-2';
        if ($_SERVER['REQUEST_URI'] == "/" . $link) echo ' active';
        echo '">' . $text . '</a>';
    }
    echo '</div></div><div class="col-md-3"></div></div><br>';
}

function nav_exercises($mysqli)
{
    $result = $mysqli->query("SELECT * FROM exercises WHERE region_id = '$_SESSION[user_region]' ORDER BY NAME;");
    if ($result->num_rows > 0) {
    ?>
        <div class="dropdown">
            <button class="btn btn-outline-primary btn-block dropdown-toggle" type="button" id="dropdownExercises" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php
                if ($_SESSION["exercise_id"] == 0) echo "Gesamtpunktzahl";
                else echo get_exercise_by_id($mysqli, $_SESSION["exercise_id"])->name;
                ?>
            </button>
            <div class="dropdown-menu btn-block text-center" aria-labelledby="dropdownExercises">
                <?php
                echo '<a class="dropdown-item';
                if ($_SESSION["exercise_id"] == 0) echo ' active';
                echo '" href="set_exercise.php?exercise_id=0">Gesamtpunktzahl</a>';
                while ($exercise = $result->fetch_object()) {
                    echo '<a class="dropdown-item';
                    if ($exercise->id == $_SESSION["exercise_id"]) echo ' active';
                    echo '" href="set_exercise.php?exercise_id=' . $exercise->id . '">' . $exercise->name . '</a>';
                }
                ?>
            </div>
        </div>
        <br>
<?php
    }
}
?>
<?php
include('inc/functions.php');
include('inc/config.php');
include('inc/frame_functions.php');
include('inc/nav_functions.php');

start($mysqli);

// Get score per region
$total_score = 0;
$score_average_per_day = 0;
$query_score_per_region = "SELECT count, CONVERT_TZ(timestamp, 'UTC', '$timezone_user') as timestamp, stats.region_id, value FROM stats LEFT JOIN exercises ON stats.exercise_id = exercises.id WHERE stats.region_id = '$_SESSION[user_region]'";
if ($_SESSION["exercise_id"] != 0) $query_score_per_region .= " AND exercise_id = '$_SESSION[exercise_id]'";
$query_score_per_region .= " ORDER BY stats.id DESC";
$result_score_per_region = $mysqli->query($query_score_per_region) or die($mysqli->error);
if ($result_score_per_region->num_rows > 0) {
    $score_array = array();
    $score_per_weekday_array = array("Montag" => 0, "Dienstag" => 0, "Mittwoch" => 0, "Donnerstag" => 0, "Freitag" => 0, "Samstag" => 0, "Sonntag" => 0);

    while ($entry = $result_score_per_region->fetch_object()) {
        $date = new DateTime($entry->timestamp);
        if (empty($score_array[$date->format('d.m.Y')])) $score_array[$date->format('d.m.Y')] = $entry->count * $entry->value;
        else $score_array[$date->format('d.m.Y')] += $entry->count * $entry->value;
        switch ($date->format('l')) {
            case "Monday":
                $score_per_weekday_array["Montag"] += $entry->count * $entry->value;
                break;
            case "Tuesday":
                $score_per_weekday_array["Dienstag"] += $entry->count * $entry->value;
                break;
            case "Wednesday":
                $score_per_weekday_array["Mittwoch"] += $entry->count * $entry->value;
                break;
            case "Thursday":
                $score_per_weekday_array["Donnerstag"] += $entry->count * $entry->value;
                break;
            case "Friday":
                $score_per_weekday_array["Freitag"] += $entry->count * $entry->value;
                break;
            case "Saturday":
                $score_per_weekday_array["Samstag"] += $entry->count * $entry->value;
                break;
            case "Sunday":
                $score_per_weekday_array["Sonntag"] += $entry->count * $entry->value;
                break;
        }
        $total_score += $entry->count * $entry->value;
    }

    // Get max. scores
    $max_all_time = 0;
    $max_per_weekday = 0;
    foreach ($score_array as $daily_score) if ($daily_score > $max_all_time) $max_all_time = $daily_score;
    foreach ($score_per_weekday_array as $daily_score) if ($daily_score > $max_per_weekday) $max_per_weekday = $daily_score;

    // Get average
    $result_first_day = $mysqli->query("SELECT CONVERT_TZ(timestamp, 'UTC', '$timezone_user') as timestamp FROM stats WHERE region_id = '$_SESSION[user_region]' ORDER BY id ASC LIMIT 1");
    $first_day = new DateTime($result_first_day->fetch_object()->timestamp);
    $result_last_day = $mysqli->query("SELECT CONVERT_TZ(timestamp, 'UTC', '$timezone_user') as timestamp FROM stats WHERE region_id = '$_SESSION[user_region]' ORDER BY id DESC LIMIT 1");
    $last_day = new DateTime($result_last_day->fetch_object()->timestamp);
    $score_average_per_day = round($total_score / ($last_day->diff($first_day)->format("%a") + 1));
}

top("Statistik - " . get_region_by_id($mysqli, $_SESSION["user_region"]));
nav(build_nav($mysqli), "Statistik");
start_main();
nav_stats($mysqli);
?>

<div class="row">
    <div class="col-md-6 offset-md-3">
        <?php
        nav_exercises($mysqli);
        ?>
        <table class="table table-sm">
            <tr>
                <td style="border-top:none">Gesamtpunktzahl</td>
                <td style="border-top:none" class="text-right"><?php echo $total_score; ?></td>
            </tr>
            <tr>
                <td>Tagesdurchschnitt</td>
                <td class="text-right"><?php echo $score_average_per_day; ?></td>
            </tr>
        </table>
        <?php
        if (isset($score_array)) {
        ?>
            <table class="table table-sm">
                <tr>
                    <th>Wochentag</th>
                    <th class="text-center">Score</th>
                    <th></th>
                </tr>
                <?php
                foreach ($score_per_weekday_array as $day => $daily_score) {
                    echo '<tr class="small">';
                    echo '<td>' . $day . '</td>';
                    echo '<td><svg width="100%" height="15px"><rect style="fill: hsl(120, 50%, 40%)" width="' . $daily_score / $max_per_weekday * 100 . '%" height="10"></rect></svg></td>';
                    echo '<td class="text-right">' . $daily_score . '</td>';
                    echo '</tr>';
                }
                ?>
            </table>
            <br>
            <table class="table table-sm">
                <tr>
                    <th>Datum</th>
                    <th class="text-center">Score</th>
                    <th></th>
                </tr>
                <?php
                foreach ($score_array as $day => $daily_score) {
                    echo '<tr class="small">';
                    echo '<td>' . $day . '</td>';
                    echo '<td><svg width="100%" height="15px"><rect style="fill: hsl(120, 50%, 40%)" width="' . $daily_score / $max_all_time * 100 . '%" height="10"></rect></svg></td>';
                    echo '<td class="text-right">' . $daily_score . '</td>';
                    echo '</tr>';
                }
                ?>
            </table>
        <?php
        } else echo '<div class="alert alert-primary" role="alert">Leider hat bislang niemand aus deiner Region ' . get_exercise_by_id($mysqli, $_SESSION["exercise_id"])->name . ' eingetragen. Es wird Zeit das zu ändern!</div>';
        echo back_button();
        ?>
    </div>
</div>

<?php
bot();
?>
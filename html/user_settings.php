<?php
include('inc/functions.php');
include('inc/config.php');
include('inc/frame_functions.php');
include('inc/nav_functions.php');

start($mysqli);

if (!empty($_POST['action'])) {
    switch ($_POST['action']) {
        case "delete_user":
            $_SESSION["alert_array"][] = delete_user($mysqli, $_SESSION['user_id']);
            header("Location: index.php?action=logout");
            exit;
        case "edit_user":
            $user = new stdClass();
            $user->id = $_SESSION["user_id"];
            if (!empty($_POST["user_name"])) $user->name = $_POST["user_name"];
            if (!empty($_POST["user_pin"])) $user->pin = $_POST["user_pin"];
            if (!empty($_POST["user_region"])) $user->region = $_POST["user_region"];
            update_user($mysqli, $user);

            // Update goals
            $result_user_goals = $mysqli->query("SELECT exercise_id, goal FROM user_goals WHERE user_id = $_SESSION[user_id];");
            while ($user_goal = $result_user_goals->fetch_object()) {
                if (isset($_POST["exercise_" . $user_goal->exercise_id]) && $_POST["exercise_" . $user_goal->exercise_id] != "") {
                    if (!is_numeric($_POST["exercise_" . $user_goal->exercise_id])) $_SESSION["alert_array"][] = array("type" => "danger", "message" => "Dein Tagesziel ist keine gültige Zahl.");
                    elseif ($_POST["exercise_" . $user_goal->exercise_id] < 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => "Dein Tagesziel für " . get_exercise_by_id($mysqli, $user_goal->exercise_id)->name . " ist zu klein.");
                    else {
                        $new_goal = $_POST["exercise_" . $user_goal->exercise_id];
                        $mysqli->query("UPDATE user_goals SET goal = '$new_goal' WHERE exercise_id = $user_goal->exercise_id AND user_id = $_SESSION[user_id]") or die($mysqli->error);
                        if ($mysqli->errno != 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => $mysqli->error);
                        else $_SESSION["alert_array"][] = array("type" => "success", "message" => "Tagesziel für " . get_exercise_by_id($mysqli, $user_goal->exercise_id)->name . " aktualisiert.");
                    }
                }
            }

            $result = $mysqli->query("SELECT * FROM users WHERE id = '$_SESSION[user_id]'");
            if ($result->num_rows > 0) {
                $user = $result->fetch_object();
                $_SESSION["user_name"] = $user->name;
                $_SESSION["user_region"] = $user->region;
            } else header("Location: index.php?action=logout");
            break;
    }
}

// Get data of current user
$result = $mysqli->query("SELECT * FROM users WHERE id = $_SESSION[user_id]");
$user = $result->fetch_object();

top("Benutzereinstellungen verwalten");
nav(build_nav($mysqli), "Accounteinstellungen");
start_main();
?>
<div class="row">
    <div class="col offset-md-3">
        <h3>Hallo <?php echo $user->name; ?>!</h3>
        <br>
        <form method="post">
            <div class="form-group">
                <label for="user_name">Nutzername</label>
                <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Nutzername" value="<?php echo $user->name; ?>" required>
            </div>
            <div class="form-group">
                <label for="user_pin">Pin (max. 10 Ziffern)</label>
                <input type="number" class="form-control" id="user_pin" aria-describedby="pin_help" name="user_pin" placeholder="Pin" min="1000" max="9999999999">
                <small id="pin_help" class="form-text text-muted">Wenn du dieses Feld leer lässt, wird dein PIN nicht geändert.</small>
            </div>
            <?php
            if ($_SESSION["user_admin_level"] == 1) {
            ?>
                <div class="form-group">
                    <label for="region_select">Region wählen:</label>
                    <select class="form-control" id="region_select" name="user_region">
                        <?php echo display_region_select($mysqli, $_SESSION['user_region']); ?>
                    </select>
                </div>
            <?php
            }
            ?>
            <br>
            <h4>Persönliche Tagesziele</h4>
            <?php
            $result_user_goals = $mysqli->query("SELECT exercise_id, goal FROM user_goals WHERE user_id = $_SESSION[user_id];");
            while ($user_goal = $result_user_goals->fetch_object()) {
            ?>
                <div class="form-group">
                    <label for="exercise_<?php echo $user_goal->exercise_id; ?>"><?php echo get_exercise_by_id($mysqli, $user_goal->exercise_id)->name; ?></label>
                    <input type="number" class="form-control" id="exercise_<?php echo $user_goal->exercise_id; ?>" name="exercise_<?php echo $user_goal->exercise_id; ?>" placeholder="<?php echo $user_goal->goal; ?>">
                </div>
            <?php
            }
            ?>
            <div class="form-group">
                <input type="hidden" name="action" value="edit_user">
                <button type="submit" class="btn btn-primary btn-block">Änderungen übernehmen</button>
            </div>
        </form>
        <form method="post">
            <div class="form-group">
                <input type="hidden" name="action" value="delete_user">
                <button type="submit" class="btn btn-danger btn-block" onclick="return confirm('Willst du deinen Account wirklich löschen? Dieser Vorgang kann nicht rückgängig gemacht werden!')">Account löschen</button>
            </div>
        </form>
        <?php echo back_button(); ?>
    </div>
    <div class="col-md-3"></div>
</div>

<?php
bot();
?>